import $ from 'jquery';
import whatInput from 'what-input';

window.$ = $;

import Foundation from 'foundation-sites';
// If you want to pick and choose which modules to include, comment out the above and uncomment
// the line below
//import './lib/foundation-explicit-pieces';


$(document).foundation();


// Set first displayed tabs to be active.
// The template iterates over all sessions before displaying only those from a specific track, so we need to ensure the first one displayed gets the appropriate classes and attributes.
if(window.location.hash) {
  // Fragment exists so do nothing because tabs are already set to be active if hash is present
} else {
  // Fragment doesn't exist meaning no specific session indicated in url so add appropriate active attributes to first session
  $( ".sessions .tabs-title:first-of-type" ).addClass( "is-active" );
  $( ".sessions .tabs-title:first-of-type" ).attr( "aria-selected", "true" );
  $( ".tabs-panel:first-of-type" ).addClass( "is-active" );
}
